//VueRouter是一个类
import Vue from 'vue'
import VueRouter from './vue-router'
import Home from '../views/Home.vue'
import About from '../views/About.vue'

Vue.use(VueRouter) //默认调用Api的install方法

const routes = [{
    path: '/home',
    component: Home
},{
    path: '/about',
    component: About
}]

export default new VueRouter({
    mode: 'hash',
    routes
})
