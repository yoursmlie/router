class VueRouter {
    constructor(options) {
        this.mode = options.mode || 'hash';
        this.routes = options.routes || [];
        //传递的路由表是一个数组{'/home': Home, '/about': About}
        this.routesMap = this.createMap(this.routes)
        //路由中需要存放当前的路径
        this.history = new HistoryRoute;
        //初始化
        this.init();
    }
    createMap(routes) {
        return routes.reduce((memo, current) => {
            memo[current.path] = current.component;
            return memo
        }, {})
    }
    init() {
        //判断mode
        if(this.mode === 'hash') {
            //判断用户打开时有没有hash，没有就跳转到'#/'
            location.hash ? '' : location.hash = '/';
            window.addEventListener('load', () => {
                this.history.current = location.hash.slice(1)
            })
            window.addEventListener('hashchange', () => {
                this.history.current = location.hash.slice(1)
            })
        }else {
            location.pathname ? '' : location.pathname = '/';
            window.addEventListener('load', () => {
                this.history.current = location.pathname
            })
            window.addEventListener('popstate', () => {
                this.history.current = location.pathname
            })
        }
    }
    go() {

    }
    back() {

    }
    push() {

    }
}

class HistoryRoute {
    constructor() {
        this.current = null
    }
}

VueRouter.install = function(Vue) {
    //每个组件都有this.$router / this.$route
    //在所有组件中获取同一个路由实例
    Vue.mixin({
        beforeCreate() {
            //定位根组件
            if(this.$options && this.$options.router) {
                this._root = this //把当前实例挂载在_root上
                this._router = this.$options.router//把router实例挂载在_router上
                //如果history中的current属性变化也会刷新视图
                Vue.util.defineReactive(this, 'xxx', this._router.history)
            }else {
                this._root = this.$parent._root
            }
            Object.defineProperty(this, '$router', {
                get() {
                    return this._root._router
                }
            })
            Object.defineProperty(this, '$route', {
                get() { //current属性
                   return {
                      //当前路由所在的状态
                      current: this._root._router.history.current
                   }
                }
            }) 
        }
    })
    Vue.component('router-link', {
        props: {
            to: String
        },
        render(h) {
            let mode = this._self._root._router.mode
            return <a href={mode === 'hash' ? `#${this.to}` : this.to}>{this.$slots.default}</a>
        }
    })
    Vue.component('router-view', { //根据当前的状态current 路由表 {'/about': About}
        render(h) {
            let current = this._self._root._router.history.current
            let routeMap = this._self._root._router.routesMap
            return h(routeMap[current])
        }
    })
}


//使用vue.use就会调用install方法
export default VueRouter